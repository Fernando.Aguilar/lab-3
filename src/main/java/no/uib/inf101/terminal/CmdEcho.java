package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    
    
    

    @Override
    public String run(String[] args) {
        String result = "";
        
        for(String str : args){
            result += str + " ";
        }
        return result;
    }

    @Override
    public String getName() {
        return "echo";
    }
    
}
