package no.uib.inf101.terminal;


public interface Command {

    /**
   * call run on Command -objects
   *
   */
    String run(String[] args);
    
    /**
   * Get comand name 
   *
   * @return  the name
   */
    String getName();

    default void setContext(Context context) {};
}
